// eslint-disable-next-line import/no-extraneous-dependencies
import { Meta, StoryObj } from '@storybook/react';
import CustomSearch from 'components/UI/CustomSearch';

const meta: Meta<typeof CustomSearch> = {
  title: 'App/Search',
  component: CustomSearch,
  tags: ['autodocs'],
};

export default meta;

export const DefaulSearch: StoryObj<typeof CustomSearch> = {
  args: {
    helpText: '',
    searchValue: '',
  },
};

export const SearchWithMistake: StoryObj<typeof CustomSearch> = {
  args: {
    helpText: 'Incorrect or incomplete pokémon name entered',
    searchValue: 'Ddddd',
  },
};
