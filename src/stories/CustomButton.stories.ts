// eslint-disable-next-line import/no-extraneous-dependencies
import { Meta, StoryObj } from '@storybook/react';
import CustomButton from 'components/UI/CustomButton';

const meta: Meta<typeof CustomButton> = {
  title: 'App/Button',
  component: CustomButton,
  tags: ['autodocs'],
};

export default meta;

export const DefaultButton: StoryObj<typeof CustomButton> = {
  args: {
    type: 'button',
    children: 'Submit',
  },
};
