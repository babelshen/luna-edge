// eslint-disable-next-line import/no-extraneous-dependencies
import { Meta, StoryObj } from '@storybook/react';
import CustomMultipleSelector from 'components/UI/CustomMultipleSelector';
import { IPokemon } from 'components/interface';

const meta: Meta<typeof CustomMultipleSelector> = {
  title: 'App/CustomMultipleSelector',
  component: CustomMultipleSelector,
  tags: ['autodocs'],
};

export default meta;

export const DefaultCustomMultipleSelector: StoryObj<typeof CustomMultipleSelector> = {
  args: {
    label: 'Team members',
    id: 'someid',
    team: [],
  },
};

export const FilledCustomMultipleSelector: StoryObj<typeof CustomMultipleSelector> = {
  args: {
    label: 'Team members',
    id: 'someid',
    team: [{ name: 'pokemon1' } as IPokemon, { name: 'pokemon2' } as IPokemon, { name: 'pokemon3' } as IPokemon, { name: 'pokemon4' } as IPokemon],
  },
};

export const CustomMultipleSelectorWithMistake: StoryObj<typeof CustomMultipleSelector> = {
  args: {
    label: 'Team members',
    id: 'someid',
    team: [],
    errorTeam: 'Fill in all the fields to take part in the battle.',
  },
};
