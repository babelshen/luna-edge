// eslint-disable-next-line import/no-extraneous-dependencies
import { Meta, StoryObj } from '@storybook/react';
import CustomInput from 'components/UI/CustomInput';
import { FieldError } from 'react-hook-form';

const meta: Meta<typeof CustomInput> = {
  title: 'App/Input',
  component: CustomInput,
  tags: ['autodocs'],
};

export default meta;

export const DefaultInput: StoryObj<typeof CustomInput> = {
  args: {
    type: 'text',
    id: 'someid',
    label: 'Name',
    placeholder: 'Type your name',
  },
};

export const InputWithMistake: StoryObj<typeof CustomInput> = {
  args: {
    type: 'text',
    id: 'someid',
    label: 'Name',
    placeholder: 'Type your name',
    error: { message: 'This field must be filled in' } as FieldError,
  },
};
