import { ICustomMultipleSelector } from './interface';

const CustomMultipleSelector: React.FC<ICustomMultipleSelector> = ({ label, errorTeam, id, team, handleButtonClick, setSelectField, selectField, handleRemoveAllBadge }) => (
  <label className="max-w-400 w-full h-full flex flex-col gap-4 font-semibold text-base" htmlFor={id}>
    {label}
    <div
      id={id}
      className={`w-full bg-white border-2 border-gray-300 rounded-md border-solid flex flex-row justify-between min-h-14 items-center active:border-blue-700 focus:border-blue-700 hover:border-blue-700 ${
        errorTeam ? 'border-orange-500' : `border-gray-300`
      }`}
    >
      <div className="overflow-hidden whitespace-nowrap py-3 px-2">
        {team.map((pokemon) => (
          <span className="py-0.5 px-2.5 mr-2 text-sm bg-neutral-200 font-normal rounded-lg inline-flex align-text-bottom gap-x-1">
            <span className="text-gray-800 mb-1">{pokemon.name}</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="currentColor"
              className="w-3 h-3 m-auto cursor-pointer"
              onClick={() => handleButtonClick(pokemon)}
            >
              <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
            </svg>
          </span>
        ))}
      </div>
      <div className="flex gap-2.5 mr-3">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-5 h-5 cursor-pointer" onClick={() => handleRemoveAllBadge()}>
          <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
        </svg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth="1.5"
          stroke="currentColor"
          className={`w-5 h-5 cursor-pointer transform transition-transform duration-300 ${selectField ? 'rotate-180' : ''}`}
          onClick={() => {
            setSelectField(!selectField);
          }}
        >
          <path strokeLinecap="round" strokeLinejoin="round" d="m19.5 8.25-7.5 7.5-7.5-7.5" />
        </svg>
      </div>
    </div>
    {errorTeam ? <p className="text-orange-500">{errorTeam}</p> : <p className="text-gray-600 font-normal">You need to choose four characters</p>}
  </label>
);

export default CustomMultipleSelector;
