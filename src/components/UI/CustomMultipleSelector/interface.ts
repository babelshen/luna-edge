import { IPokemon } from 'components/interface';

export interface ICustomMultipleSelector {
  label: string;
  errorTeam: string;
  id: string;
  team: IPokemon[];
  handleButtonClick: (pokemon: IPokemon) => void;
  setSelectField: (param: boolean) => void;
  selectField: boolean;
  handleRemoveAllBadge: () => void;
}
