import { MouseEventHandler, ReactNode } from 'react';

export interface ICustomButton {
  type: 'submit' | 'reset' | 'button';
  children: ReactNode;
  onClick: MouseEventHandler<HTMLButtonElement>;
}
