export interface ICustomSearch {
  helpText: string;
  searchValue: string;
  setSearchValue: (param: string) => void;
}
