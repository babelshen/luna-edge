import { ICustomSearch } from './interface';

const CustomSearch: React.FC<ICustomSearch> = ({ helpText, searchValue, setSearchValue }) => (
  <div className={`w-full bg-white border-2 hover:border-blue-700 active:border-blue-700 focus:border-blue-700 ${helpText ? `border-orange-500` : `border-gray-300`} rounded-md flex items-center`}>
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className={`w-6 h-6 pl-2 ${helpText ? `text-orange-500` : `text-black`}`}>
      <path strokeLinecap="round" strokeLinejoin="round" d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z" />
    </svg>

    <input
      type="text"
      value={searchValue}
      placeholder="Type name of pokemon"
      className="w-full bg-transparent p-3 pl-2 border-none rounded-md focus:border-none focus:outline-none"
      onChange={(e) => {
        setSearchValue(e.target.value);
      }}
    />
  </div>
);

export default CustomSearch;
