import React from 'react';
import { FieldError } from 'react-hook-form';

const CustomInput = React.forwardRef(
  ({ type, id, label, placeholder, error, ...props }: { type: string; id: string; label: string; placeholder: string; error?: FieldError }, ref: React.Ref<HTMLInputElement>) => (
    <label htmlFor={id} className="max-w-400 w-full flex flex-col gap-2 font-semibold text-base">
      {label}
      <div
        className={`w-full bg-white border-2 rounded-md flex items-center hover:border-blue-700 focus:border-blue-700 active:border-blue-700 ${
          error?.message ? `border-orange-500` : `border-gray-300`
        }`}
      >
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className={`w-7 h-7 pl-2 ${error?.message ? `text-orange-500` : `text-black`}`}>
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="m11.25 11.25.041-.02a.75.75 0 0 1 1.063.852l-.708 2.836a.75.75 0 0 0 1.063.853l.041-.021M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9-3.75h.008v.008H12V8.25Z"
          />
        </svg>
        <input type={type} placeholder={placeholder} id={id} ref={ref} className="w-full bg-transparent p-3 pl-2 rounded-md focus:outline-none focus:border-none" {...props} />
      </div>
      {error ? <p className="text-orange-500">{error.message}</p> : <p className="text-gray-600 font-normal">This information is required</p>}
    </label>
  ),
);

export default CustomInput;
