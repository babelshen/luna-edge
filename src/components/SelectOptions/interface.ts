import { IPokemon } from 'components/interface';

export interface ISelectOptions {
  listEntities: IPokemon[];
  setOffset: React.Dispatch<React.SetStateAction<number>>;
  team: IPokemon[];
  isLoading: boolean;
  searchValue: string;
  setSearchValue: (param: string) => void;
  helpText: string;
  clickedButtons: number[];
  handleButtonClick: (pokemon: IPokemon) => void;
}
