import { IPokemon } from 'components/interface';
import InfiniteScroll from 'react-infinite-scroll-component';
import CustomSearch from 'components/UI/CustomSearch';
import { ISelectOptions } from './interface';

const SelectOptions: React.FC<ISelectOptions> = ({ listEntities, setOffset, team, isLoading, searchValue, setSearchValue, helpText, clickedButtons, handleButtonClick }) => (
  <>
    <CustomSearch helpText={helpText} searchValue={searchValue} setSearchValue={setSearchValue} />

    {helpText ? <p className="text-orange-500">{helpText}</p> : false}

    <InfiniteScroll
      dataLength={listEntities.length}
      next={() => setOffset((prevOffset: number) => prevOffset + 20)}
      hasMore={team.length < 4 && isLoading}
      loader={<h4 className="text-center mt-3 font-semibold">Loading all characters while scrolling</h4>}
      scrollableTarget="selectFieldScroll"
    >
      {listEntities.length >= 1 ? (
        <div className="grid small:grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-5 p-5 max-h-[50vh] overflow-y-scroll border border-gray-300" id="selectFieldScroll">
          {listEntities.map((pokemon: IPokemon) => (
            <button
              type="button"
              key={pokemon.id}
              onClick={() => handleButtonClick(pokemon)}
              className="max-w-[300px] max-h-[300px] overflow-hidden bg-white p-1.5 flex flex-col justify-between	items-center border border-gray-400	border-solid group"
              style={{
                background: clickedButtons.includes(pokemon.id) ? 'yellow' : 'white',
              }}
            >
              <div className="max-w-[200px] max-h-[200px]">
                <img className="w-full h-full" src={pokemon.sprites.other.dream_world!.front_default} alt="Pokemon" />
              </div>
              <div>{pokemon.name}</div>
            </button>
          ))}
        </div>
      ) : (
        false
      )}
    </InfiniteScroll>
  </>
);

export default SelectOptions;
