import ModalWindow from 'components/ModalWindow';
import CustomButton from 'components/UI/CustomButton';
import { useState } from 'react';

const Result: React.FC = () => {
  const [openModalWindow, setOpenModalWindow] = useState<boolean>(false);

  const handleButton = () => {
    setOpenModalWindow(!openModalWindow);
  };

  return (
    <>
      {openModalWindow ? <ModalWindow handleButton={handleButton} /> : false}
      <div className="flex flex-col items-center gap-2.5 my-56 mx-auto p-2 bg-gray-200 h-30 small:w-8/12 w-6/12 rounded-xl justify-center">
        <p className="text-xl text-center font-bold">Please, wait start battle</p>
        <CustomButton type="button" onClick={handleButton}>
          View my team
        </CustomButton>
      </div>
    </>
  );
};

export default Result;
