import { useAppSelector } from 'utilities/hooks';
import { IModalWindow } from './interface';

const ModalWindow: React.FC<IModalWindow> = ({ handleButton }) => {
  const player = useAppSelector((state) => state.team);
  return (
    <div className="absolute z-10 bg-gray-900/50 top-0 left-0 right-0 bottom-0">
      <div className="w-9/12 mx-auto my-10 bg-gray-200 p-3">
        <div className="flex justify-between mb-4">
          <h3 className="font-bold text-2xl">Your team</h3>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6 cursor-pointer" onClick={handleButton}>
            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18 18 6M6 6l12 12" />
          </svg>
        </div>
        {player.team.map((pokemon) => (
          <div key={pokemon.id} className="grid grid-cols-2 items-center text-center p-2 my-2 bg-white border-2 border-gray-300 rounded-md">
            <div className="flex items-center">
              <div>{pokemon.name}</div>
              <div className="max-w-20 max-h-20 mx-auto">
                <img className="w-full h-full" src={pokemon.sprites.other!.dream_world!.front_default} alt="Pokemon" />
              </div>
            </div>
            <div className="text-left">
              <div className="flex small-big:flex-col small-big:gap-y-1 small-big:items-start items-center gap-x-1">
                <span className="font-bold">Abilities:</span>
                {pokemon.abilities.map((item, index) => (
                  <span className="py-0.5 px-2.5 text-sm bg-neutral-200 font-normal rounded-lg inline-flex align-text-bottom whitespace-nowrap" key={item.ability.name + index}>
                    {item.ability.name}
                  </span>
                ))}
              </div>
              <div>
                <span className="font-bold">Experience:</span> {pokemon.base_experience}
              </div>
              <div>
                <span className="font-bold">Weight:</span> {pokemon.weight}
              </div>
              <div>
                <span className="font-bold">Height:</span> {pokemon.height}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ModalWindow;
