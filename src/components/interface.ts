export interface IPokemon {
  id: number;
  name: string;
  abilities: IAbility[];
  base_experience: number;
  height: number;
  weight: number;
  sprites: {
    other: {
      dream_world?: {
        front_default: string;
      };
    };
  };
}

interface IAbility {
  ability: {
    name: string;
    url: string;
  };
  is_hidden: boolean;
  slot: number;
}
