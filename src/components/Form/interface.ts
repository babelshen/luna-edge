import { IPokemon } from 'components/interface';

export interface IForm {
  name: string;
  lastName: string;
  team: IPokemon[];
}
