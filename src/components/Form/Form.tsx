import { useForm } from 'react-hook-form';
import { useState } from 'react';
import { useAppDispatch } from 'utilities/hooks';
import { addPokemonToTeam } from 'store/createTeam/actionCreators';
import { IPokemon } from 'components/interface';
import CustomButton from 'components/UI/CustomButton';
import CustomInput from 'components/UI/CustomInput';
import SelectComponent from '../SelectComponent';
import { IForm } from './interface';

const Form: React.FC = () => {
  const dispatch = useAppDispatch();

  const [team, setTeam] = useState<IPokemon[]>([]);
  const [errorTeam, setErrorTeam] = useState<string>('');

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IForm>({ mode: 'onBlur' });

  const onSubmit = (data: Omit<IForm, 'team'>) => {
    if (team.length === 4 && data.name && data.lastName) {
      const result = { ...data, team };
      dispatch(addPokemonToTeam(result));
    } else setErrorTeam('Fill in all the fields to take part in the battle.');
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="w-9/12 mx-auto bg-gray-200 border-2 border-gray-400 rounded-md flex flex-col items-center p-5 gap-4">
      <CustomInput
        type="text"
        id="username"
        label="Name"
        placeholder="Type your name"
        error={errors.name}
        {...register('name', {
          required: 'This field must be filled in',
          minLength: {
            value: 2,
            message: 'Name is too short. Minimum characters: 2',
          },
          maxLength: {
            value: 12,
            message: 'Name is too long. Maximum characters: 12',
          },
          pattern: {
            value: /^[a-zA-Z]+$/,
            message: 'Invalid value',
          },
        })}
      />

      <CustomInput
        type="text"
        id="userlastname"
        label="Last Name"
        placeholder="Type your last name"
        error={errors.lastName}
        {...register('lastName', {
          required: 'This field must be filled in',
          minLength: {
            value: 2,
            message: 'Last name is too short. Minimum characters: 2',
          },
          maxLength: {
            value: 12,
            message: 'Last name is too long. Maximum characters: 12',
          },
          pattern: {
            value: /^[a-zA-Z]+$/,
            message: 'Invalid value',
          },
        })}
      />

      <SelectComponent team={team} setTeam={setTeam} errorTeam={errorTeam} setErrorTeam={setErrorTeam} />

      <CustomButton type="submit" onClick={handleSubmit(onSubmit)}>
        Submit
      </CustomButton>
    </form>
  );
};

export default Form;
