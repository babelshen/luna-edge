import axios from 'axios';
import { useEffect, useState } from 'react';
import SelectOptions from 'components/SelectOptions';
import { IPokemon } from 'components/interface';
import { useDebounce } from 'utilities/useDebounce';
import CustomMultipleSelector from 'components/UI/CustomMultipleSelector';
import { IGeneralPokemonInfo, ISelectComponent } from './interface';

const SelectComponent: React.FC<ISelectComponent> = ({ team, setTeam, errorTeam, setErrorTeam }) => {
  const [listEntities, setListEntities] = useState<IPokemon[]>([]);
  const [clickedButtons, setClickedButtons] = useState<number[]>([]);
  const [offset, setOffset] = useState<number>(0);
  const [initialLoad, setInitialLoad] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>('');
  const [selectField, setSelectField] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [helpText, setHelpText] = useState<string>('');

  const debaunceValue = useDebounce(searchValue, 500);

  useEffect(() => {
    if (initialLoad) {
      if (searchValue) {
        setListEntities([]);
        setOffset(0);
        setIsLoading(false);
        axios
          .get(`https://pokeapi.co/api/v2/pokemon/${searchValue}`)
          .then((response) => {
            setListEntities([response.data]);
            setHelpText('');
          })
          .catch((error) => {
            console.log(error);
            setHelpText('Incorrect or incomplete pokémon name entered');
            setListEntities([]);
          });
      } else {
        setIsLoading(true);
        axios
          .get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`)
          .then((response) => {
            const requests = response.data.results.map((result: IGeneralPokemonInfo) => axios.get(result.url));
            return Promise.all(requests);
          })
          .then((responses) => {
            const result = responses.map((item) => item.data);
            setListEntities((prevListEntities) => [...prevListEntities, ...result]);
            setHelpText('');
          })
          .catch((e) => {
            console.log(e);
            setHelpText('Something wrong! Try assembling a Pokémon team later');
            setIsLoading(false);
          });
      }
    } else {
      setInitialLoad(true);
    }
  }, [offset, initialLoad, searchValue, debaunceValue]);

  const handleButtonClick = (pokemon: IPokemon) => {
    const exist = team.find((entity) => entity.id === pokemon.id);
    if (exist) {
      setTeam((prevTeam: IPokemon[]) => prevTeam.filter((entity) => entity.id !== pokemon.id) as IPokemon[]);
      setClickedButtons((prevClickedButtons) => prevClickedButtons.filter((buttonKey) => buttonKey !== pokemon.id));
      setErrorTeam('');
    } else if (team.length < 4) {
      setTeam((prevTeam: IPokemon[]) => [...prevTeam, pokemon] as IPokemon[]);
      setClickedButtons((prevClickedButtons) => [...prevClickedButtons, pokemon.id]);
      setErrorTeam('');
    } else {
      setErrorTeam('The team has already been recruited');
    }
  };

  const handleRemoveAllBadge = () => {
    setTeam([]);
    setClickedButtons([]);
    setErrorTeam('');
  };

  return (
    <>
      <CustomMultipleSelector
        label="Team members"
        errorTeam={errorTeam}
        id="team_members"
        team={team}
        handleButtonClick={handleButtonClick}
        setSelectField={setSelectField}
        selectField={selectField}
        handleRemoveAllBadge={handleRemoveAllBadge}
      />

      {selectField ? (
        <SelectOptions
          listEntities={listEntities}
          setOffset={setOffset}
          team={team}
          isLoading={isLoading}
          searchValue={searchValue}
          setSearchValue={setSearchValue}
          helpText={helpText}
          clickedButtons={clickedButtons}
          handleButtonClick={handleButtonClick}
        />
      ) : (
        false
      )}
    </>
  );
};

export default SelectComponent;
