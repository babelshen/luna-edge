import { IPokemon } from 'components/interface';

export interface ISelectComponent {
  team: IPokemon[];
  setTeam: React.Dispatch<React.SetStateAction<IPokemon[]>>;
  errorTeam: string;
  setErrorTeam: (param: string) => void;
}

export interface IGeneralPokemonInfo {
  name: string;
  url: string;
}
