import Form from 'components/Form';
import Result from 'components/Result';
import { useAppSelector } from 'utilities/hooks';

const App: React.FC = () => {
  const player = useAppSelector((state) => state.team);
  return <div className="mx-8 bg-gray-300 py-16">{player.team.length === 4 ? <Result /> : <Form />}</div>;
};

export default App;
