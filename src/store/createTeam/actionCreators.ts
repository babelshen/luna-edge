import { AppDispatch } from 'store/store';
import { IForm } from 'components/Form/interface';
import { addToTeam, clearTeam } from './teamSlice';

export const addPokemonToTeam = (data: IForm) => (dispatch: AppDispatch) => {
  dispatch(addToTeam(data));
};

export const clearTeamAction = () => (dispatch: AppDispatch) => {
  dispatch(clearTeam());
};
