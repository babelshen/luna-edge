import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IForm } from 'components/Form/interface';

const initialState: IForm = {
  name: '',
  lastName: '',
  team: [],
};

const teamSlice = createSlice({
  name: 'team',
  initialState,
  reducers: {
    addToTeam: (state, action: PayloadAction<IForm>) => {
      state.name = action.payload.name;
      state.lastName = action.payload.lastName;
      state.team = [...action.payload.team];
    },
    clearTeam: (state) => {
      state.name = '';
      state.lastName = '';
      state.team = [];
    },
  },
});

export const { addToTeam, clearTeam } = teamSlice.actions;

export default teamSlice.reducer;
