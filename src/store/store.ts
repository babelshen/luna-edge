import { configureStore } from '@reduxjs/toolkit';
import teamSlice from './createTeam/teamSlice';

export const store = configureStore({
  reducer: {
    team: teamSlice,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
